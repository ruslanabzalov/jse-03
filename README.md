# JSE TASK MANAGER

## About Application
Task Manager Application

## About Developer
* **Full Name**: Ruslan Abzalov
* **E-Mail**: ruslanonetwo@gmail.com

## Hardware
* **CPU**: Intel Core i7-7500U @ 2.70GHz x 4
* **RAM**: 8GB
* **Disk Capacity**: SSD 256GB

## Software
* **OS Name**: Ubuntu 20.04.1 LTS
* **OS Type**: 64-bit
* **JDK**: 1.8+

## How To Run The Application
* Open project in IntelliJ IDEA
* Mark `resource` directory as a `Resource`
* Create `JAR` artifacts
* Build project
* Build artifacts
* Execute this command in terminal:
```
$ java -jar task-manager.jar
```

## Tasks Results
* **JSE-00**: [jse-00](https://drive.google.com/drive/folders/1vwK52B_H550TXXs2pLqFJfb83yswBuj4?usp=sharing)
* **JSE-01**: [jse-01-task-manager-result.png](https://drive.google.com/file/d/1w3qYGjnhBTM0jpmxhF_G_OPoruwfJD99/view?usp=sharing)
* **JSE-02**: [jse-02-task-manager-result.png](https://drive.google.com/file/d/1h1hVvMiSoqK0iVvXO9F1ESBWBAJN5K8H/view?usp=sharing)
* **JSE-03**: [jse-03](https://drive.google.com/drive/folders/14YWFA2tSnaPmDJH-tx6XaPdG_ElknKYD?usp=sharing)